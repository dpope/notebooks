# Labeled Graph
G=nx.Graph()

#add nodes
for i in range(5):
    G.add_node(i)

#edges
G.add_edge(0,1)
G.add_edge(0,4)
G.add_edge(1,2)
G.add_edge(1,4)
G.add_edge(2,3)
G.add_edge(3,4)
G.add_edge(3,5)
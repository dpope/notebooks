#!/usr/bin/env python3
"""This module contains all the classes for the  NetworkEncyclopedia and WONetworkEncyclopedia filetypes"""
from __future__ import absolute_import, division, print_function, unicode_literals
from builtins import (bytes, str, open, super, range, zip, round, input, int, pow, object)
class NetworkEncyclopedia:
    """ Network Encylcopedia filetype class. For both WONetworkEncyclopedia and NetworkEncyclopedia"""
    def __init__(self, inpfilename):
        self.filename=inpfilename
        self.line=openandread(self.filename)
        self.filelen=len(self.line)
        self.reaction=[]
        self.reactionnum=0
        self.measurementnum=0

    def readfiletype(self):
        if (self.filename==filename1):
            self.filetype=1
            self.readfile1()
        elif (self.filename==filename2):
            self.filetype=2
            self.readfile2()
        elif (self.filename==filename3):
            self.filetype=3
            self.readfile3()
        else:
            import sys
            sys.exit('Filename of %s not recognized' % self.filename)
    
    def readfile1(self):
        """ This function is for reading files formatted as NetworkEncyclopedia.txt"""
        self.linenum=0
        self.flowcontrol=0
# flowcontrol -1: rxn read incorrectly, continue until next . found
# flowcontrol 0: rxn not read 
# flowcontrol 1 rxn read, no valid measurements 
# flowcontrol 2: rxn read, valid measurements
        while self.linenum<self.filelen:
            if self.line[self.linenum].strip()=='':
                pass
            elif self.line[self.linenum].strip()=='.':
                if self.flowcontrol==1:
                    self.reactionnum+=1
                self.flowcontrol=0
            elif self.flowcontrol==-1:
                pass
            elif self.flowcontrol==0:
                self.rxnstr=self.line[self.linenum].strip()
                self.measurementnum=0
                self.rxnlinenum=self.linenum
                self.flowcontrol=1
            elif self.flowcontrol==1 and self.includemeasurement(self.line[self.linenum]):
                self.flowcontrol=2
                self.addreaction(self.rxnstr)
                if self.flowcontrol==2:
                    self.addmeasurement(self.line[self.linenum])
                else:
                    self.measurementnum=0
            elif self.flowcontrol==1:
                self.measurementnum+=1
            elif self.flowcontrol==2:
                self.addmeasurement(self.line[self.linenum])
            else:
                import sys
                sys.exit('The readfile1 function of Class:NetworkEncyclopedia failed')
            self.linenum+=1

    def readfile2(self):
        """This function is for reading files formatted as WONetworkEncyclopedia.txt"""
        self.linenum=0
        self.flowcontrol=0
# flowcontrol 0: rxn not read 
# flowcontrol 1 read first measurement of rxn
# flowcontrol 2 read rest of measurements of rxn
        while self.linenum<self.filelen:
            if self.line[self.linenum].strip()=='':
                import sys
                sys.exit('Blank line found in %s on line %s ' % (filename2,self.linenum))
            elif self.line[self.linenum].strip()=='.':
                self.flowcontrol=0
            elif self.flowcontrol==0:
                self.rxnstr=self.line[self.linenum].strip()
                self.rxnlinenum=self.linenum
                self.addreaction(self.rxnstr)
                self.flowcontrol=1
            elif self.flowcontrol==1:
                self.reactionnum=self.findreactionnnumfile2(self.line[self.linenum])
                self.reaction[-1].reactionnum=self.reactionnum
                self.addmeasurement(self.line[self.linenum])
            elif self.flowcontrol==2:
                self.addmeasurement(self.line[self.linenum])
            else:
                import sys
                sys.exit('The readfile2 function of Class:NetworkEncyclopedia failed')
            self.linenum+=1

    def readfile3(self):
        """This function is for reading files formatted as SavedNetworkEncyclopedia.txt"""
# flowcontrol 0: vertices not read
# flowcontrol 1: reading vertices 
# flowcontrol 1: reading solutions
        self.linenum=0
        self.flowcontrol=0
        self.vertices=[]
        self.numvertices=0
        while self.linenum<self.filelen:
            if self.flowcontrol==0:
                if 'identity of vertices' in self.line[self.linenum]:
                    self.flowcontrol=1
            elif self.flowcontrol==1:
                if 'solutions for vertices' in self.line[self.linenum]:
                    self.flowcontrol=2
                else:
                    vertexnum=int(self.line[self.linenum].strip())
                    atctid=self.line[self.linenum+1].strip()
                    casrn=self.line[self.linenum+2].strip()
                    phase=self.line[self.linenum+3].strip()
                    if atctid==casrn: #and phase.lower()=='g':
                        atctid+='*0'
                    name=self.line[self.linenum+4].strip()
                    self.vertices.append(Species(name,self.linenum))
                    self.vertices[-1].addverticie(vertexnum,atctid,casrn,phase,name)
                    self.numvertices+=1
                    if self.vertices[-1].vertexid!=self.numvertices:
                        from sys import exit
                        exit('inconsistency in vertices reading')
                    self.linenum+=4
            elif self.flowcontrol==2:
                if 'Qfun provenance for vertices' in self.line[self.linenum]:
                    self.flowcontrol=3
                else:
                    vertexnum, hf, unc =self.line[self.linenum].split()
                    vertexnum=int(vertexnum)-1
                    hf=hf.replace('D','E')
                    unc=unc.replace('D','E')
                    # subtract one for index here, to get actual vertex number
                    self.vertices[vertexnum].hfid(hf,unc)
            elif self.flowcontrol==3:
                return
            else:
                import sys
                sys.exit('The readfile3 function of Class:NetworkEncyclopedia failed')
            self.linenum+=1

    def addreaction(self, inpline):
        self.reactionnum+=1
        self.reaction.append(Reaction(inpline,self.filetype,self.rxnlinenum,self.reactionnum))
        if self.reaction[-1].successparsing==-1:
            print ('unable to parse rxn line %s of %s. All measurements of this reaction are skipped \
                    \n SKIPPING %s num %s '\
                    % (self.linenum, self.filename, self.line[self.linenum-1],self.reactionnum))
            del self.reaction[-1]
            self.flowcontrol=-1

    def includemeasurement(self, inpline):
# All measurements of WONetworkEncyclopedia are included
        if self.filetype==2:
            return True
        else:
            line=inpline.split('//')
# Adding a hack here to include it based on bracket parsing. This is due to a curious bug in the ATcT Parser
            if len(line)>1 and line[-1].strip()!='':
                from pyparsing import printables,Suppress,Word,Group,Optional,nums,ParseException
                lbra='['
                rbra=']'
                printables_less_bra= printables.replace(lbra,'').replace(rbra,' ')
                species = Word(printables_less_bra) + Optional(Suppress(lbra) + Word(printables_less_bra) + Suppress(rbra)) + Optional(Word(printables_less_bra))
                expr=  species
                listofnote=expr.parseString(line[-1]).asList() 
                if len(listofnote)>1:
                    return True
                else:
                    pass
            line=line[0]
            line=line.split()
            if len(line)>6:
# tests if measurement in NetworkEncyclopedia has a weight > 0 for inclusion
                try:
                    typofixed=line[6].replace('*','.')
                    if typoprint==1:
                        if typofixed!=line[6]:
                            print('Typo on line %s of %s with * for reaction weight rather \
                                    than . \n TYPO FIXED AND INCLUDED %s' \
                                    % (self.linenum,self.filename,self.line[self.linenum]))
                    if float(typofixed)>0.01:
                        return True
                    else:
                        return False
                except ValueError:
                    print('error parsing %s on line %s \n SKIPPING %s)'\
                            %(self.filename,self.linenum,self.line[self.linenum]))
                    return False
            elif len(line)>4 and len(line)<7:
                return True
            else:
                import sys
                sys.exit('error with includemeasurement parsing %s on line %s\n %s'\
                        % (self.filename,self.linenum,self.line[self.linenum]))

    def addmeasurement(self, inpline):
        self.measurementnum+=1
        if self.filetype==2:
            self.measurementnum=self.findmeasurementnumfile2(inpline)
        if self.includemeasurement(inpline):
            self.reaction[-1].addmeasurement(inpline,self.measurementnum)

    def findreactionnnumfile2(self,inpline):
        reactionnumandmeasurementnum=inpline.split()[-1]
        return int(reactionnumandmeasurementnum.split('.')[0])

    def findmeasurementnumfile2(self,inpline):
        reactionnumandmeasurementnum=inpline.split()[-1]
        return int(reactionnumandmeasurementnum.split('.')[1])

    def printmeasurements(self):
        for x in self.reaction:
            x.printmeasurement()

    def removeignoredreactions(self,logfile):
        """This function opens a seperate file (SolveLog.log) from ATcT solution and uses it to 
        remove some reactions from NE, with no regard for other validation"""
        self.loglines=openandread(logfile)
        ignoredreactions=[]
        for x in range(len(self.loglines)):
            if 'Reaction has been ignored' in self.loglines[x]:
                rxntoignore=int(self.loglines[x+1].strip().split('>>')[0])
                self.deletereaction(rxntoignore)
                ignoredreactions.append(rxntoignore)
        print ('The following reactions were deleted based on ' +  logfile + ' from ' + self.filename + ' '+str(ignoredreactions) + '\n')

    def deletereaction(self,rxnnumber):
        for x in range(len(self.reaction)):
            if self.reaction[x].reactionnum==rxnnumber:
                del self.reaction[x]
                break
                return

    def removeignoredmeasurements(self,logfile):
        """This function opens a seperate file (SolveLog.log) from ATcT solution and should use it to 
        remove some reactions from NE, with no regard for other validation, but the reason I don't delete
        these is Because the ATcT Parser actually includes the last DH removed, so I chose to remove them later"""
        ignoredmeasurements=[]
        for x in range(len(self.loglines)):
            if 'Entry ignored: temperature not in range of available partition function data' in self.loglines[x]\
                    or 'Entry ignored: internal temp. not in range of available partition function data' in self.loglines[x]:
                ignoredmeasurement=self.loglines[x-1].strip().split('>>')[0]
#                self.deletemeasurement(ignoredmeasurement)
                ignoredmeasurements.append(ignoredmeasurement)
        print ('The following mesurements should be deleted based on ' +  logfile + ' from ' + self.filename + ' '+str(ignoredmeasurements) + '\n')

    def NEtoTNMatrix(self):
        """This function returns the TN is matrix formulation Not implemented yet"""
        pass
#        speciesset=()
#        for x in range(len(self.reaction)):
#            for y in range(len(self.reaction[x])):
#            print(self.reaction[x].reactant)


class Reaction:
    def __repr__(self): 
        return repr([str(self.reactantcoef), str(self.reactant), '->', str(self.productcoef), str(self.product)])

    def __init__(self, reactionstr,filetype,linenum,reactionnum):
        self.reactionstr=reactionstr
        self.filetype=filetype
        self.linenum=linenum
        self.reactionnum=reactionnum
        self.measurement=[]
        self.successparsing=self.parserxnstr()

    def addmeasurement(self, inpstr,measurementnum):
        if self.filetype==1:
            notefound=False
            try:
                notedivide=inpstr.strip().split('//')
                if len(notedivide)>1:
                    note=notedivide[1]
                    notefound=True
                line=notedivide[0].split()
                if len(line)>6 or (len(line)>4 and len(line)<7):
                    value=line[0]
                    uncertainty=line[1]
                    temperature=line[2]
                    unit=line[3]
                    typeofrxn=line[4]
                    reflist=line[5].replace('[','').replace(']','').split(',')
                else:
                    sys.exit('error with %s on line %s' % (self.filetype,inpstr))
            except ValueError:
                print('error parsing addmeasurement SKIPPING %s)' % inpstr)
                return
        elif self.filetype==2:
            line1,line2,line3=self.parsemeasureline(inpstr)
            line1=line1.split()
            value=line1[0]
            uncertainty=line1[1]
            temperature=line1[2]
            unit=line1[3]
            try:
                reflist=line2.split(':')[1].split(',')
            except IndexError:
                reflist=[]
            uncmult=line3.split()[0]
        if len(temperature)==3 and int(temperature)==298:
            temperature=str(298.15)
        self.measurement.append(Measurement(self.reactionstr, value, uncertainty, temperature, unit, measurementnum))
        for x in reflist:
            try:
                self.measurement[-1].reference.append(int(x))
            except ValueError:
                pass
        if self.filetype==1: 
            self.measurement[-1].typeofrxn=typeofrxn
            if notefound:
                self.measurement[-1].note=note
# This was added to find exceptions
#                if not self.measurement[-1].parsenote():
#                    print (self.linenum)
        elif self.filetype==2: 
            self.measurement[-1].uncertaintymultipler=uncmult

    def printmeasurement(self):
        print(self.reactionstr)
        for x in self.measurement:
            print(x)

    def nicereaction(self):
        outstr=''
        for x in range(len(self.reactant)):
            if x>0:
                outstr=outstr+' + '
            if self.reactantcoef[x]!=str(1):
                outstr=outstr+self.reactantcoef[x]+'&nbsp;' 
            outstr=outstr+self.reactant[x].name+'&nbsp;' 
            outstr=outstr+'('+self.reactant[x].phase+') '
        outstr=outstr+' &rarr; '
        for x in range(len(self.product)):
            if x>0:
                outstr=outstr+' + '
            if self.productcoef[x]!=str(1):
                outstr=outstr+self.productcoef[x]+'&nbsp;' 
            outstr=outstr+self.product[x].name+'&nbsp;' 
            outstr=outstr+'('+self.product[x].phase+') '
        self.nicereaction=outstr

    def wordreaction(self,document,roman):
        p = document.add_paragraph()
        p.add_run('Table S-' + roman+ ': Selected reactions of ')
        for x in range(len(self.reactant)):
            if x>0:
                p.add_run(' + ')
            if self.reactantcoef[x]!=str(1):
                p.add_run(self.reactantcoef[x])
            ion=False
            for y in self.reactant[x].name:
                sub=p.add_run(y)
                if y=='+' or y=='-':
                    ion=True
                if ion:
                    sub.font.superscript=True
                try:
                    int(y)
                    sub.font.subscript=True
                except ValueError:
                    pass

            #p.add_run(self.reactant[x].name)
            sub=p.add_run('('+self.reactant[x].phase+') ')
            sub.font.subscript=True
        p.add_run(' '+'\u2192' + ' ')
        for x in range(len(self.product)):
            if x>0:
                p.add_run(' + ')
            if self.productcoef[x]!=str(1):
                p.add_run(self.productcoef[x])
            for y in self.product[x].name:
                sub=p.add_run(y)
                if y=='+' or y=='-':
                    ion=True
                if ion:
                    sub.font.superscript=True
                try:
                    int(y)
                    sub.font.subscript=True
                except ValueError:
                    pass
            sub=p.add_run('('+self.product[x].phase+') ')
            sub.font.subscript=True
        p.add_run(' included in ATcT')
        return
    

    def parserxnstr(self):
        self.reactantcoef=[]
        self.reactant=[]
        self.productcoef=[]
        self.product=[]
        if len(self.reactionstr)>stringmaxlength:
            return -1
        rxnstr=self.reactionstr.replace(' + ',',,')
        rxnstr=rxnstr.replace(' +{',',, {')
        rxnstr=rxnstr.replace('}+','} ,,')
        rxnstr=rxnstr.split('->')
        reactants=rxnstr[0].split(',,')
        products=rxnstr[1].split(',,')
        for x in reactants:
            self.reactant.append(Species(x,self.linenum))
            if parsereaction==1:
                self.reactantcoef.append(self.reactant[-1].parserxnstrandreturncoef())
                if self.reactantcoef[-1]==-1:
                    return -1
        for x in products:
            self.product.append(Species(x,self.linenum))
            if parsereaction==1:
                self.productcoef.append(self.product[-1].parserxnstrandreturncoef())
                if self.productcoef[-1]==-1:
                    return -1

    def parsemeasureline(self,inp):
        from pyparsing import printables,Suppress,Word,Group,Optional,nums,ParseException,OneOrMore
        lbra, rbra='[', ']'
        printables_less= printables.replace(rbra,'').replace(lbra,' ')
        species = Word(printables_less) + Suppress(lbra) + Optional(Word(printables_less)) \
                + Suppress(rbra) +  Optional(Word(printables))
        expr=  species
        try:
            outlist= expr.parseString(inp).asList()
            if len(outlist)==2:
                outlist.insert(1,'no ref')
            return outlist
        except ParseException:
            print (inp)
            import sys
            sys.exit('Pyparsing Error in parsemeasureline')


    def fetchatctid(self):
        for x in range(len(self.reactant)):
            oldatctid=self.reactant[x].atctid
            self.reactant[x].atctid=self.reactant[x].returnatctid()
            if self.reactant[x].atctid==-1:
                if printatctiderrors==1:
                    print('No Valid ATcTID queried for {}, keeping the written atctid: {} in {}'.\
                            format(str(self.reactant[x].nameandphase),str(oldatctid),str(self.reactionstr)))
                self.reactant[x].atctid=oldatctid 
            elif self.reactant[x].atctid!=oldatctid and printatctiderrors==1:
                print('Inconsistency between queried {} and written atctid {} for {} in {}'.\
                        format(self.reactant[x].atctid,oldatctid,str(self.reactant[x].name),str(self.reactionstr)))
            else:
                pass
        for x in range(len(self.product)):
            oldatctid=self.product[x].atctid 
            self.product[x].atctid=self.product[x].returnatctid()
            if self.product[x].atctid==-1:
                if printatctiderrors==1:
                    print('No Valid ATcTID queried for {}, keeping the written atctid: {} in {}'.\
                            format(str(self.product[x].nameandphase),str(oldatctid),str(self.reactionstr)))
                self.product[x].atctid=oldatctid 
            elif self.product[x].atctid!=oldatctid and printatctiderrors==1:
                print('Inconsistency between queried {} and written atctid {} for {} in {}'.\
                        format(self.product[x].atctid,oldatctid,str(self.product[x].name),str(self.reactionstr)))
            else:
                pass

class Species:        
    """ Species Class; used to find ATcTID's: extensive queries to SQL database here."""
    def __repr__(self): 
        return str(self.atctid)

    def __init__(self,species,linenum):
        self.species=species
        self.linenum=linenum

    def addverticie(self,vertexnum,atctid,casrn,phase,name):
        self.vertexid=vertexnum
        self.atctid=atctid
        self.casrn=casrn
        self.name=name
        self.phase=phase

    def hfid(self,hf,unc):
        self.hf=hf
        self.unc=unc

    def parserxnstrandreturncoef(self):
        """ Seperate species from coefficent and its markup using parsespecies function."""
        parsed=self.parsespecies(self.species)
        if parsed==-1:
            return -1
        else:
            nameandphase=parsed[0][1].strip()
            reactioncoef=parsed[0][0].strip()
# Seperate phase and name from species  using parsephase function
        self.seperatenameandphase(nameandphase,1)
        if self.atctid==-1:
            return -1
# Return the coefficent of the species in the Reaction for storage
        try:
            if int(reactioncoef)>maxcoefnum:
                reactioncoef=str(maxcoefnum)
        except ValueError:
            pass
        return reactioncoef

    def seperatenameandphase(self,inp,optionalreadatctid):
        self.nameandphase=inp
        parsedspecies=self.parsephase(inp)
        self.name=parsedspecies[0].strip()
        if 'cyc-t' in self.name:
            self.name=self.name.replace('cyc-t','')
        if 'cyc-' in self.name:
            self.name=self.name.replace('cyc-','')
        self.phase=parsedspecies[1].strip()
        if optionalreadatctid==1:
            if len(parsedspecies)>2 and checkallatctid==0:
                tmpid=parsedspecies[2].strip().split(',')
                if '*' in tmpid[0]:
                    self.atctid=tmpid[0]
                elif tmpid[0]:
                    self.atctid=tmpid[0]+'*0'
#                    print('added star0 to %s' % self.atctid)
                else:
                    import sys
                    sys.exit('error in Species:seperatenameandphase reading %s' % inp)
                if len(tmpid)>1 and printatctiderrors==1:
                    print ('ATcTID: %s contains additional information' % parsedspecies[2].strip())
            elif len(parsedspecies)>2:
                self.atctid=self.returnatctid()
                if self.atctid!=parsedspecies[2].strip() and self.atctid!=(parsedspecies[2].strip()+'*0'):
                    if self.atctid!=-1:
                        print ('Found ATcTID {} vs given {} in {} on line {}'.format\
                                (self.atctid,parsedspecies[2].strip(),self.species,self.linenum))
                    else:
                        print ('Unable to find given ATcTID {} from {} on line {}'.format\
                                (parsedspecies[2].strip(),self.species,self.linenum))
            else:
                self.atctid=self.returnatctid()

    def parsespecies(self,inp):
        from pyparsing import printables,Suppress,Word,Group,Optional,nums,ParseException
        lcbr='{'
        rcbr='}'
        fslash='/'
        printables_less_cbr= printables.replace(lcbr,'').replace(rcbr,' ')
        species = Suppress(lcbr) + Word(printables_less_cbr) + Suppress(rcbr)
        coef=nums+fslash
        integer= Word(coef)
        species_with_prefix= Group(Optional (integer, default="1") + species) 
        expr=  species_with_prefix
        try:
            return expr.parseString(inp).asList()
        except ParseException:
# Error printing now handled upstream to give more relevant information
#            print ('SKIPPING all entries under %s due to ParseError in Parsespecies' % inp)
            return -1

    def parsephase(self,inp):
        from pyparsing import printables,Suppress,Word,Group,Optional,nums,ParseException
        lt='<'
        gt='>'
        printables_less= printables.replace(lt,'').replace(gt,' ')
        species = Word(printables_less) + Suppress(lt) + Word(printables_less) \
                + Suppress(gt) + Optional( Suppress(',') + Word(printables_less))
        expr=  species
        try:
            return expr.parseString(inp).asList()
        except ParseException:
            print (inp)
            import sys
            sys.exit('Pyparsing Error in parsephase')

    def findcharge(self):
        if '+' in self.name:
            tmpcharge=self.name.split('+')
            if tmpcharge[1]=='':
                return 1
            else:
                return int(tmpcharge[1])
        elif '-' in self.name:
            tmpcharge=self.name.split('-')
            if tmpcharge[1]=='':
                return -1
            else:
                return int(tmpcharge[1])
        else:
            return 0

    def returnatctid(self):
        from db_connect import MyDB
        con=MyDB()
        modstar0=0
        if self.phase=='' or self.phase=='g':
            modstar0=1
        if modstar0==1:
            sdquery= "SELECT sd.casrn FROM speciesdict sd where sd.name = %s"
            con.query(sdquery, [self.name])
            results=con.fetch()
            resultslist=[]
            for x in results:
                resultslist.append(x[0]+'*0')
            resultslist_uniques=list(set(resultslist))
            if len(resultslist_uniques)==1:
                atctquery= "SELECT s.atctid FROM species s where s.atctid=%s"
                params=resultslist_uniques[0]
            else:
                atctquery= "SELECT cb.atctid  FROM  speciescookbook cb \
                        join speciesdict sd on sd.casrn=cb.sd_casrn \
                        where sd.name = %s group by cb.id"
                params=self.name
        else:
            atctquery= "SELECT cb.atctid  FROM speciescookbook cb \
                    join speciesdict sd on sd.casrn=cb.sd_casrn \
                    where sd.name = %s group by cb.id"
            params=self.name
        con.query(atctquery,[params])
        results=con.fetch()
        flowcontrol=0
# try one thing after another until the query returns success!
        while len(results)>1 or len(results)==0:
            if flowcontrol==0:
                self.charge=self.findcharge()
                atctquery= "SELECT s.atctid FROM species s join speciescookbook cb\
                        on cb.chemid=s.id join speciesdict sd on sd.casrn=cb.sd_casrn \
                        where sd.name = %s AND CONCAT_WS(', ',s.phase,s.additional_formula_descriptor) \
                        = %s AND s.charge= %s group by s.id"
                params=[self.name,self.phase,self.charge]
            elif flowcontrol==1:
# Try the cookbook
                atctquery= "SELECT cb.atctid FROM speciescookbook cb where cb.name = %s"
                params=[self.nameandphase.strip().split('>')[0]+'>']
            elif flowcontrol==2:
# Assume that it is a chemical formula and try to find the exact chemical match; with charge
                from chemicalformulaparse import parseformula,ele2eleid 
                ele2elid=ele2eleid()
                parsedformula=parseformula([self.name])
                eles=parsedformula[0]
                numofeles=parsedformula[1]
                count=0
                for y in range(len(eles)):
                    if eles[y] in ele2elid:
                        if count==0:
                            query_elements = ("join chem2ele ce0 on s.id = ce0.chemid \
                                    and ce0.eleid= '{}' and ce0.numofele = '{}' ".\
                                    format(ele2elid[eles[y]],numofeles[y]))
                        else:
                            query_elements = query_elements + \
                                    ("join chem2ele ce{} on ce{}.chemid =ce{}.chemid \
                                    and  ce{}.eleid= '{}' and ce{}.numofele = '{}' ".\
                                    format(count,int(count-1),count,count,ele2elid[eles[y]],count,numofeles[y]))
                    count += 1
                atctquery =  "SELECT s.atctid from species s " + query_elements + \
                        "AND s.numuniqueelements=%s AND s.charge=%s AND \
                        s.phase like %s"
                params=[len(eles),self.charge,("%" + self.phase.split()[0] + "%")]
            elif flowcontrol==3 and modstar0==1:
# This ignores the phaseline if it was empty or only contained gas and instead queries for ATcT internal ID=0 (*0; for default gas)
                atctquery =  "SELECT s.atctid from species s " + query_elements +\
                        "AND s.numuniqueelements=%s AND s.charge=%s AND\
                        s.additional_atctid = %s"
                params=[len(eles),self.charge,0]
            elif flowcontrol==4 and modstar0==1:
# Very permissive query to find the gas phase species assuming modstar0 holds
                atctquery =  "SELECT s.atctid from species s join speciescookbook cb on cb.chemid=s.id \
                        join speciesdict sd on sd.casrn=cb.sd_casrn where sd.name = %s AND s.phase like %s group by s.id" 
                params=[self.name,("%g%") ]
            elif len(results)>1:
                if querydebug==1:
                    print (results)
                    print ('ATcTid not found for {}: {} {}'.format(self.nameandphase,self.name,self.phase))
                return -1
            elif(len(results)==0):
                if querydebug==1:
                    print ('ATcTid not found for {}'.format(self.nameandphase))
                return -1
            flowcontrol+=1
            con.query(atctquery,params)
            results=con.fetch()
        return results[0][0]


class Measurement:
    """ Parses a measurement string so it can be added to SQL db"""
    def __repr__(self): 
        returnlist=[self.value, self.uncertainty, self.temperature, self.unit]
# Add optional elements if they exist
        try:
            returnlist.append(self.typeofrxn)
            returnlist.append(self.note)
        except AttributeError:
            pass
        if len(self.reference)>0:
            returnlist.append(self.reference)
        return repr(returnlist)

    def __str__(self): 
        returnstr=str(self.value).ljust(9)+ ' '+ str(self.uncertainty).ljust(9)+ ' ' +str(self.temperature).ljust(9)+ ' ' +str(self.unit).ljust(9)
# Add optional elements if they exist
        try:
            returnstr = returnstr + ' ' +str(self.typeofrxn)
        except AttributeError:
            pass
        if len(self.reference)>0:
            returnstr = returnstr + ' ' +str(self.reference).replace(' ','')
        try:
            returnstr = returnstr + ' //' +str(self.note)
        except AttributeError:
            pass
        return returnstr

    def __init__(self,reaction, value, uncertainty, temperature, unit, measurementnum):
        self.reaction=reaction
        self.value=value
        self.uncertainty=uncertainty
        self.temperature=temperature
        self.unit=unit
        self.measurementnum=measurementnum
        self.reference=[]

    def parsenote(self):
        from pyparsing import printables,Suppress,Word,Group,Optional,nums,ParseException
        lcbr='['
        rcbr=']'
        printables_less_cbr= printables.replace(lcbr,'').replace(rcbr,' ')
        species = Word(printables_less_cbr) + Optional(Suppress(lcbr) + Word(printables_less_cbr) + Suppress(rcbr)) + Optional(Word(printables_less_cbr))
        expr=  species
        try:
            listofnote=expr.parseString(self.note).asList() 
            if len(listofnote)>1:
#                print ('found brackets '+ self.note)
                return False
            else:
                return True
        except ParseException:
            print ('SKIPPING all entries under %s due to ParseError in Parsenote' % self.note)
        return True
#            return -1


def unittestspecies(tests):
    """ A testing function to test species class quickly"""
    speciesstr=['CH2CCH <g>']
    atctidexpected=['2932-78-7*0']
    global db
    version='1.118'
    import re
    pattern=re.compile('[\D]+')
    db='atctv'+pattern.sub('',version) 
    global typoprint, parsereaction, checkallatctid, querydebug
    querydebug=2 # If querydebug=1 then the failed queries are printed, if >1 then ALL queries are printed
    con=MyDB()
    linenums=[]
    species=[]
    for x in range(len(speciesstr)):
        linenums.append(x)
        species.append(Species(speciesstr[x],linenums[x]))
        species[x].seperatenameandphase(speciesstr[x],0)
        species[x].atctid=species[x].returnatctid
        if species[x].atctid!=atctidexpected[x]:
            print ('error on %s expected %s and found %s' % (speciesstr[x],atctidexpected[x],species[x].atctid))
        else:
            print ('Success on %s expected %s and found %s' % (speciesstr[x],atctidexpected[x],species[x].atctid))


def openandread(filename):
    import sys,os.path
    if os.path.isfile(filename):
        txt=open(filename,'r')
    else:
        sys.exit('file not found')
    try:
        lines=txt.readlines()
    except UnicodeDecodeError:
        import codecs
        txt = codecs.open(filename,'r',encoding='latin1')
        lines=txt.readlines()
    for x in range(len(lines)):
        lines[x]=lines[x].encode('utf8').decode('utf8').strip()
    return lines

def ne_parse_settings(typoprintin, parsereactionin, checkallatctidin, querydebugin, printatctiderrorsin, stringmaxlengthin, maxcoefnumin):
    """Function to set globals in this module so that proper settings are used on importing."""
# parameters
    global typoprint, parsereaction, checkallatctid, querydebug, printatctiderrors, stringmaxlength
    global maxcoefnum
    typoprint=typoprintin
    parsereaction=parsereactionin # If parsereaction=1 then reaction strings are parsed
    querydebug=querydebugin # If querydebug=1 then the failed queries are printed, if >1 then ALL queries are printed
    checkallatctid=checkallatctidin # if checkallatctid!=0 then all atctids are checked via query 
    printatctiderrors=printatctiderrorsin # if printatctiderrors=1, print the errors, otherwise ignore
    stringmaxlength=stringmaxlengthin # number of characters that can be read in a reaction string not useful because ATcT's is size based on charlen
    maxcoefnum=maxcoefnumin # Number of coefficent preceeding species in reactions ATcT parser has a hardlimit on this
    printfixedatctids=printatctiderrorsin # If printfixedatctids=1 then mismatches between NE and WONE are printed

def ne_filenames(filename1in='NetworkEncyclopedia.txt',filename2in='WONetworkEncyclopedia.txt',filename3in='SavedNetworkEncyclopedia.txt'):
    global filename1,filename2,filename3
    filename1=filename1in
    filename2=filename2in
    filename3=filename3in
# I don't want to open and close the db connection...
    global db

def main():
    pass

if __name__=="__main__":
    main()

#!/usr/bin/env python
"""This module was written to read the output of the Variance Decomposition. It is python2 and python3 compatable. 
Requires the Adjacency Matrix **Amat**, the original reactions *Yvec*, and the original uncertainties *zvec* from Variance Decomposition, which itself gets them from SavedNetworkEncyclopedia.txt. 

"""

from __future__ import absolute_import, division, print_function, unicode_literals
from builtins import (bytes, str, open, super, range, 
                              round, input, int, pow, object) # need python2 zip
import math


class VarDecomp:
    """ This class is used to read the output of the Variance Decomposition program with the following printing options

logical,parameter          ::  DUMP_Species= .true.
logical,parameter          ::  DUMP_trial_Enthalpies= .true.
logical,parameter          ::  DUMP_A_MAT= .true.
logical,parameter          ::  DUMP_Reactions= .true.  """
    def __init__(self):
        self.species=[]
        self.reactions=[]
        self.lines=[]
        self.linenum=0
        self.flowcontrol=0
        self.originalenthapliesread=0
        self.originalenthapliesread2=0
        self.subnetvertices=[]
        self.subnetedges=[]
        self.adjacencylist=[]
        self.diagofhat=[]

    def ATcTunclist(self):
        """The uncertainties are read from ATcT. The minimum uncertainty comes from ATcT's value and is minuncertainty=1.196265649770218e-05"""
        uncertaintylist=[]
        minuncertainty=1.196265649770218e-05
        for x in self.reactions:
            if x.uncertainty<minuncertainty:
                unc=minuncertainty
            else:
                unc=x.uncertainty
            try:
                uncertaintylist.append(unc*x.ATcTmultiplier)
            except AttributeError:
                uncertaintylist.append(unc)
        for x in range(self.nterminators):
            if (self.species[x+self.nvertices].unc<minuncertainty):
                unc=minuncertainty
            else:
                unc=self.species[x+self.nvertices].unc
            uncertaintylist.append(unc)
        return uncertaintylist
            
    def measurementlist(self):
        measurementlist=[]
        for x in self.reactions:
            measurementlist.append(x.enthalpy)
        for x in range(self.nterminators):
            measurementlist.append(self.species[x+self.nvertices].dh)
        return measurementlist

    def input_var_decomp(self):
        import sys
        if len(sys.argv)>1:
            return sys.argv[1]
        else:
            return input('give the input filename: ')

    def parse(self):
# Flowcontrol parsing structure
# flowcontrol = 0 : Nothing read, next should read size of matrices
# flowcontrol = 1 : Read the matrices sizes, time to read subnet sizes
# flowcontrol = 2 : subnets read, reading species
# flowcontrol = 3 : species read, reading trial enthalpies of formation
# flowcontrol = 4 : trial enthalpies read, reading Adjacency Matrix
# flowcontrol = 5 : adjacency matrix read, reading heats of formation
        while self.linenum < len(self.lines):
            if self.flowcontrol==0:
                tmp=self.lines[self.linenum].split()
                self.nvertices=int(tmp[0])
                self.nedges=int(tmp[2])
                self.nterminators=int(tmp[4])
                self.nsubnets=int(tmp[6])
                self.flowcontrol+=1
            elif self.flowcontrol==1:
                subnetdef=self.lines[self.linenum].split()
                self.subnetvertices.append(int(subnetdef[2]))
                self.subnetedges.append(int(subnetdef[4])-1)
                if len(self.subnetvertices)==self.nsubnets:
                    self.flowcontrol+=1
                    self.linenum+=1
            elif self.flowcontrol==2:
                tmpspecies=self.lines[self.linenum].split()
                tmpdescriptor=' '.join(tmpspecies[2:])
                self.species.append(Species(tmpspecies[0],tmpspecies[1],tmpdescriptor))
                if len(self.species)==self.nvertices+self.nterminators:
                    self.flowcontrol+=1
                    self.linenum+=1
            elif self.flowcontrol==3:
                tmpvalues=self.lines[self.linenum].split()
                tmpdh=float(tmpvalues[1].replace('D','e'))
                tmpunc=float(tmpvalues[2].replace('D','e'))
                self.species[self.originalenthapliesread].add_trial_values(tmpdh,tmpunc)
                self.originalenthapliesread+=1
                if self.originalenthapliesread==len(self.species):
                    self.flowcontrol+=1
                    self.linenum+=2
            elif self.flowcontrol==4:
                tmpstoich=self.lines[self.linenum].split()
                self.reactions.append(Reaction())
                if len(tmpstoich)!=self.nvertices+self.nterminators:
                    from sys import exit
                    exit('error in parsing line %s' % self.linenum)
                for x in range(len(tmpstoich)):
                    self.adjacencylist.append(float(tmpstoich[x]))
                    if abs(float(tmpstoich[x]))>0.1:
                        self.reactions[-1].addreactant(x,tmpstoich[x])
                if len(self.reactions)==self.nedges:
                    self.flowcontrol+=1
                    self.linenum+=1
            elif self.flowcontrol==5:
                tmpvalues=self.lines[self.linenum].split()
                tmprxnid=tmpvalues[1]
                try:
                    tmpdh=float(tmpvalues[2].replace('D','e'))
                    tmpunc=float(tmpvalues[4].replace('D','e'))
                except ValueError:
                    print(self.linenum,self.lines[self.linenum],tmpvalues)
                    from sys import exit
                    exit()
                self.reactions[self.originalenthapliesread2].add_enthalpy(tmprxnid,tmpdh,tmpunc)
                if len(tmpvalues)>5:
                    tmpuncmult=float(tmpvalues[6].replace('D','e'))
                    self.reactions[self.originalenthapliesread2].add_uncertmult(tmpuncmult)
                self.originalenthapliesread2+=1
                if self.originalenthapliesread2==len(self.reactions):
                    break
            else:
                print(self.species[-1].dh)
                from sys import exit
                exit('Parse of Variance Decomposition failed')
            self.linenum+=1

    def printreactions(self):
        for i in range(len(self.reactions)):
            self.printreaction(i)

    def printreaction(self,i):
        spec=self.reactions[i].species
        reactant=''
        product=''
        for j in range(len(spec)):
            if (self.reactions[i].coef[j])<0:
                if self.reactions[i].coef[j]<-.99 and self.reactions[i].coef[j]>-1.01:
                    reactant=reactant+ self.species[spec[j]].descriptor+ ' '
                else:
                    reactant=reactant+str(self.reactions[i].coef[j])+ self.species[spec[j]].descriptor+ ' '
            else:
                if self.reactions[i].coef[j]>.99 and self.reactions[i].coef[j]<1.01:
                    product=product+ self.species[spec[j]].descriptor+ ' '
                else:
                    product=product+str(self.reactions[i].coef[j])+ self.species[spec[j]].descriptor+ ' '
        print(self.reactions[i].ATcTrxnID + ' ' + reactant + ' -> ' + product)


class Species:
    """ ATcT species with an input number, the ATcTID, and the description"""
    def __repr__(self):
        return self.ATcTID

    def __init__(self,number,ATcTID,descriptor):
        self.number=int(number)-1
        self.ATcTID=ATcTID
        self.descriptor=descriptor

    def add_trial_values(self,dh,unc):
        self.dh=dh
        self.unc=unc

class Reaction:
    """ Contains species in their stoichometric equivalents for a reaction"""
    def __repr__(self):
        return self.ATcTrxnID

    def __init__(self):
        self.species=[]
        self.coef=[]

    def addreactant(self,id,coef):
        """ Add the species """
        self.species.append(id)
        self.coef.append(float(coef))

    def add_enthalpy(self,rxnID,enthalpy,uncertainty):
        """ Sets the enthalpy and its uncertaintiy for a reaction"""
        self.ATcTrxnID=rxnID
        self.enthalpy=enthalpy
        self.uncertainty=uncertainty

    def add_uncertmult(self,ATcTmultiplier):
        self.ATcTmultiplier=ATcTmultiplier


def openandread(filename):
    """ Reads a file, using codecs to avoid errors from latin1 encoding"""
    import sys,os.path
    if os.path.isfile(filename):
        txt=open(filename,'r')
    else:
        print(filename)
        sys.exit('file not found')
    try:
        lines=txt.readlines()
    except UnicodeDecodeError:
        import codecs
        txt = codecs.open(filename,'r',encoding='latin1')
        lines=txt.readlines()
    for x in range(len(lines)):
        lines[x]=lines[x].encode('utf8').decode('utf8').strip()
    return lines

def vec2matrix(inpvec,nrow,mcol):
    """ takes an input vector,nrows,mcols  and returns a numpy array with those dimensions"""
    import numpy as np
    returnmatrix=np.array(inpvec,eval(numpy_precision))
    returnmatrix=returnmatrix.reshape(nrow,mcol)
    returnmatrix.shape
    return returnmatrix

def SVDinv(matrix):
    """ Returns inverse using SVD"""
    minval=1.11e-16
    import numpy as np
    U, s, v=np.linalg.svd(matrix,full_matrices=False)
    lstar=np.zeros(s.shape,dtype=eval(numpy_precision))
#Remove singularities from s
    for x in np.nditer(s, op_flags=['readwrite']):
        if x>minval:
            x[...]=(np.divide(1.0,x))
        else:
            x[...]=0
    inv=np.dot(v.transpose(),np.dot(np.diag(s).transpose(),U.transpose())) 
    return (inv)


def SVDsolve(Amat,Yvec,Zvec,usecovar=False,colmean=False):
    """ Use the standard SVD approach for an over determined system, using numpy.linalg svd (SVD from LAPACK routine _gesdd)
    options to subtract column means of weighted matrix (changes solution in some cases) and to use the full covariance matrix instead (similar to ATcT's solution). Note that using the full covariance matrix actually gives a answer more different than using SVD to solve the over determined system itself."""
    minval=1.11e-16
    import numpy as np
    m, n=Amat.shape
    if usecovar:
        AdjT=Amat.transpose()
        Vmat=np.diag(np.divide(1.,np.square(Zvec)))
        Covar=np.dot(np.dot(AdjT,Vmat),Amat)
        bvec=np.asarray(np.dot(np.dot(AdjT,Vmat),Yvec)).reshape(-1)
        U, s, v=np.linalg.svd(Covar,full_matrices=False)
    else:
        Vmat=np.diag(np.reciprocal(Zvec))
        Adjw=np.dot(Vmat,Amat)
        if colmean:
            col_mean=np.mean(Adjw,axis=0)
            Adjw=np.subtract(Adjw,np.tile(col_mean,(m,1)))
        Yvecw=np.dot(Vmat,Yvec)
#  Check to see if shuffling the rows does anything, it doesn't, good!
#        Adjw,Yvecw=shuffle_in_unison(Adjw,Yvecw)
        U, s, v=np.linalg.svd(Adjw,full_matrices=False)
    lstar=np.zeros(s.shape,dtype=eval(numpy_precision))
#Remove singularities from s
    for x in np.nditer(s, op_flags=['readwrite']):
        if x>minval:
            x[...]=(np.divide(1.0,x))
        else:
            x[...]=0
    Covar1=np.dot(v.transpose(),np.dot(np.diag(s).transpose(),U.transpose()))
    if usecovar:
        hf=np.asarray(np.dot(Covar1,bvec)).reshape(-1)
        if condition_num_print:
            print('Condition Number of solved = %s [norm calc] %s [SVD]' % \
                    (np.linalg.norm(Covar)*np.linalg.norm(Covar1),np.ndarray.max(s)/np.ndarray.min(s)))
    else:
        hf=np.asarray(np.dot(Covar1,Yvecw)).reshape(-1)
        if condition_num_print:
            print('Condition Number of solved = %s [norm calc] %s [SVD]' % \
                    (np.linalg.norm(Adjw)*np.linalg.norm(Covar1),np.ndarray.max(s)/np.ndarray.min(s)))
#    print('SVD sum((I-A*A)^2= %s:%s' % \
#            (np.sum(np.square(np.subtract(np.identity(Covar1.shape[0],dtype=eval(numpy_precision)),np.dot(Covar,Covar1))))\
#            ,np.sum(np.square(np.subtract(np.identity(Covar1.shape[0],dtype=eval(numpy_precision)),np.dot(Covar,Covar1))))))
    if usecovar:
        unc=np.sqrt(np.diag(Covar1))
    else:
        truecovar=np.dot(np.dot(v.transpose(),np.diag(np.square(s))),v)
# Technically it should be divided by this, but results poor on doing so
#        if n>1:
#            unc=np.sqrt(np.diag(truecovar/(n-1)))
#        else:
        unc=np.sqrt(np.diag(truecovar))
    if solvehatmat:
        errorvec=np.abs(np.subtract(Yvec,np.dot(Amat,hf)))
        residual=np.sum(np.square(errorvec))
        if m==n:
            errorcovar=SVDinv(np.dot(truecovar.transpose(),truecovar))*residual
        else:
            errorcovar=SVDinv(np.dot(truecovar.transpose(),truecovar))*residual/(m-n)
        if np.any(np.greater(errorvec,Zvec)):
            print('error in errorvec')
            print(errorvec)
            from sys import exit
            exit()
        hatmatrix=np.dot(np.dot(Adjw,truecovar),Adjw.transpose())
        diagofhat=np.diag(hatmatrix)
#       CDF of hatmatrix
#        sorted_data = np.sort(diagofhat)
#        yvals=np.arange(len(sorted_data))/float(len(sorted_data)-1)
#        import matplotlib.pyplot as plt
#        plt.figure()
#        plt.xlim(0, 1)
#        plt.plot(sorted_data,yvals)
#        plt.show(block=False)
#        from scipy.interpolate import splrep, splantider, splev
#        plt.figure()
#        spline=splrep(yvals,sorted_data, k=3,s=1e-7)
#        yfit=splev(yvals,spline)
#        plt.xlim(0, 1)
#        plt.plot(yfit,yvals)
#        plt.show(block=False)
#        plt.figure()
#        plt.xlim(0, 1)
#        yder=splev(yvals,spline,der=1)
#        plt.plot(yder,yvals,linestyle='solid')#,marker='o')
#        plt.title('Derivitive')
#        plt.show(block=True)
        Inf_v_unc=np.divide(Zvec,diagofhat)
#        print(diagofhat)
#        avginf=float(n)/float(m)
        avginf=float(1)/float(m)
        locs=np.nonzero(Amat)
#        print([locs])
#        print(col_mean)
        k=0
#        for i in range(0):
        for i in range(m):
            if np.abs(diagofhat[i])>(n/m):
                species=np.ravel(np.nonzero(Amat[i]))
                terminal_species=False
                k+=1
                for j in range(len(species)): 
                    count=np.count_nonzero(np.equal(locs[1],species[j]))
                    if count==1:
                        terminal_species=True
                        break
                if terminal_species==False:
                    pass
#                    print(i)
#        print(k,m,n)
#        for i in range(m):
#            Mmat_subi=np.outer(Adjw[i],Adjw[i])
#            Leave_one_out_covar=np.add(truecovar,(np.dot(np.dot(truecovar,Mmat_subi),truecovar)/(np.subtract(1,diagofhat[i]))))
##            print(np.square(np.diag(Adjw[i])))
##            Leave_one_out_covar=np.add(truecovar,np.divide(np.dot(np.dot(truecovar,np.dot(Adjw[i],Adjw[i].transpose())),truecovar)/(np.subtract(1,diagofhat[i]))))
#            new_unc=np.sqrt(np.diag(Leave_one_out_covar))
#            diff=np.subtract(new_unc,unc)
##            if i==4:
##                print(unc)
##                print(new_unc)
##                print(diff)
##        print(np.sum(np.diag(hatmatrix)))
##        EV, Evec=(np.linalg.eig(hatmatrix))
##        print(EV)
        maxval=np.max(hatmatrix)
        diagsum=np.sum(np.diag(hatmatrix))
        if maxval>1.001:
            print('Max value of hatmatrix %s indicates errors' % np.max(hatmatrix))
        if abs(diagsum-n)>0.01:
            print ('Sum of hatmatrix diag %s indicates errors' % diagsum)
        return (hf,unc,truecovar,hatmatrix)
    else:
        return (hf,unc,truecovar)

def submatrix_preparse():
    D=VarDecomp()
    D.inpfile=D.input_var_decomp()
    D.lines=openandread(D.inpfile)
    D.parse()
    del D.lines
    del D.linenum
    del D.adjacencylist
    del D.flowcontrol
    del D.originalenthapliesread
    del D.originalenthapliesread2
    import numpy as np
    initialedge=0
    initialvertex=0
    measuredvec=D.measurementlist()
    uncvec=D.ATcTunclist()
    for x in range(1):
#    for x in range(D.nsubnets):
        AdjArray=np.zeros((D.subnetedges[x]+D.nterminators+1,D.subnetvertices[x]+D.nterminators),dtype=eval(numpy_precision))
        finalvertex=initialvertex+D.subnetvertices[x]
        finaledge=initialedge+D.subnetedges[x]+1
        terminatorshift=-len(D.species)+D.nterminators+D.subnetvertices[x]
        for y in range(initialedge,finaledge):
            for z in range(len(D.reactions[y].species)):
                if ((D.reactions[y].species[z]-initialvertex)>D.subnetvertices[x]):
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)+terminatorshift]=D.reactions[y].coef[z]
                else:
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)]=D.reactions[y].coef[z]
        for y in range(D.nterminators):
           AdjArray[finaledge+y][finalvertex+y]=1.0
        if D.subnetedges[x]>=D.subnetvertices[x]:
            N=D.subnetvertices[x]
            M=D.subnetedges[x]
            measuredvecinp=measuredvec[initialedge:finaledge]+measuredvec[D.nedges:D.nedges+D.nterminators]
            uncvecinp=uncvec[initialedge:finaledge]+uncvec[D.nedges:D.nedges+D.nterminators]
            Yvec=np.array(measuredvecinp,dtype=eval(numpy_precision))
            Zvec=np.array(uncvecinp,dtype=eval(numpy_precision))
            Hfrec, Hfunc, truecovar =SVDsolve(AdjArray,Yvec,Zvec) 
            Vmat=np.diag(np.divide(1.,Zvec))
            Adjw=np.dot(Vmat,AdjArray)
            hatmatrix=np.dot(np.dot(Adjw,truecovar),Adjw.transpose())
            diagofhat=np.diag(hatmatrix)[0:M]
            D.Hfrec=Hfrec.tolist()
            D.Hfunc=Hfunc.tolist()
#            D.truecovar=truecovar.tolist()
            D.diagofhat=diagofhat.tolist()
    txt=open('hatmat.json','w')
    import jsonpickle
    txt.write(jsonpickle.encode(D))
#    import json
#    json.dumps(D,txt)
#    pickle.dump(D,txt, protocol=pickle.HIGHEST_PROTOCOL)

def submatrix_mean_distance(weighted=False):
    """ Calculate mean distance in graph"""
    import numpy as np
    np.set_printoptions(suppress=True,threshold=np.nan,linewidth=np.nan)
    import os.path
    import sys
    initialedge=0
    initialvertex=0
    hatmatdone=False
    if os.path.isfile('hatmat.json'):
        if len(sys.argv)>1: 
            D=VarDecomp()
            D.inpfile=D.input_var_decomp()
            D.lines=openandread(D.inpfile)
            D.parse()
        else:
            import jsonpickle
            txt=open('hatmat.json','r')
            D=jsonpickle.decode(txt.read())
            hatmatdone=True
    measuredvec=D.measurementlist()
    uncvec=D.ATcTunclist()
    for x in range(1):
#    for x in range(D.nsubnets):
        AdjArray=np.zeros((D.subnetedges[x]+D.nterminators+1,D.subnetvertices[x]+D.nterminators),dtype=eval(numpy_precision))
        finalvertex=initialvertex+D.subnetvertices[x]
        finaledge=initialedge+D.subnetedges[x]+1
        terminatorshift=-len(D.species)+D.nterminators+D.subnetvertices[x]
        for y in range(initialedge,finaledge):
            for z in range(len(D.reactions[y].species)):
                if ((D.reactions[y].species[z]-initialvertex)>D.subnetvertices[x]):
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)+terminatorshift]=D.reactions[y].coef[z]
                else:
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)]=D.reactions[y].coef[z]
        for y in range(D.nterminators):
           AdjArray[finaledge+y][finalvertex+y]=1.0
        if D.subnetedges[x]>=D.subnetvertices[x]:
            N=D.subnetvertices[x]
            M=D.subnetedges[x]
            measuredvecinp=measuredvec[initialedge:finaledge]+measuredvec[D.nedges:D.nedges+D.nterminators]
            uncvecinp=uncvec[initialedge:finaledge]+uncvec[D.nedges:D.nedges+D.nterminators]
            Yvec=np.array(measuredvecinp,dtype=eval(numpy_precision))
            Zvec=np.array(uncvecinp,dtype=eval(numpy_precision))
            if not hatmatdone:
                Vmat=np.diag(np.divide(1.,Zvec))
                Hfrec, Hfunc, truecovar =SVDsolve(AdjArray,Yvec,Zvec) 
                Adjw=np.dot(Vmat,AdjArray)
                hatmatrix=np.dot(np.dot(Adjw,truecovar),Adjw.transpose())
                diagofhat=np.diag(hatmatrix)[0:M]
            else:
                Hfrec=np.array(D.Hfrec,dtype=eval(numpy_precision))
                Hfunc=np.array(D.Hfunc,dtype=eval(numpy_precision))
                diagofhat=np.array(D.diagofhat,dtype=eval(numpy_precision))
            from itertools import chain
            vertex_names = [ D.species[i].descriptor for i in chain(range(initialvertex,finalvertex), range(D.nvertices,D.nvertices+D.nterminators)) ]
            minval=10E-3
            import igraph 
            termlist = []
            for y in range(D.nterminators):
                termlist.append(D.species[len(D.species)-1-y].descriptor)
            keepspecies= set()
            import sys
            cutoff_hat = 0.1
            n_edges=0
            diagofhat2=np.copy(diagofhat)
            inflist=np.flipud(np.argsort(diagofhat2))
            edgelist=[]
            i=0                         
            while i < len(inflist):
                conn_indices = np.where(AdjArray[inflist[i]]) # get the row, col indices of the non-zero elements in your adjacency matrix
                edgelist.append(inflist[i])
                n_edges+=1
                for j in conn_indices:
                    for k in j:
                        keepspecies.add(k)
                i+=1            
            #add terminators that contain elements in common w/ the species of interest
            newtermlist = []
            
            for term in termlist:
                 for k in range(len(vertex_names)):
                     edgelist.append(k)
                     conn_indices = np.where(AdjArray[k])
                     n_edges+=1
                     for j in conn_indices:
                         for p in j:
                             keepspecies.add(p)
                     keepspecies.add(k)
                     newtermlist.append(term)
            termlist = newtermlist          
            keepspecies=sorted(list(keepspecies))
            G = igraph.Graph(directed=True)
            G.add_vertices(max(keepspecies)+1+n_edges)
            weight_new=[]
            edge_count=-1
            edge_names=[]
            edge_infl=[]
            reactionlist=[]
            keepset=set()
            edgeindex=[]
            nskip=0
            stoich_new=[]
            for i in edgelist:
                edge_count+=1
                conn_indices = np.where(AdjArray[i]) # get the row, col indices of the non-zero elements in your adjacency matrix
                edges = list(zip(*conn_indices)) # a sequence of (i, j) tuples, each corresponding to an edge from i -> j
                stoich=[]
                for j in range(len(edges)):
                    edges[j]=int(edges[j][0])
                    stoich.append((AdjArray[i][edges[j]]))
                j=0
                edges2=[]
                stoich2=[]
                while j<len(edges): 
                    if AdjArray[i,edges[j]]<0:
                        stoich2.append(stoich[j])
                        edges2.append(edges[j])
                        del(edges[j])
                        del(stoich[j])
                    else:
                        j+=1
                if len(edges)>0 and len(edges2)>0:
                    if (edges,edges2) not in reactionlist:
#                        edge_names.append('')
                        edge_names.append(str(D.reactions[i].ATcTrxnID))
#                        edge_names.append(D.reactions[i].ATcTrxnID)
                        edge_infl.append([diagofhat[i]])
                        for k in range(len(edges)):
                            keepset.add(edges[k])
                            keepset.add(max(keepspecies)+1+edge_count) 
                            reactionlist.append((edges,edges2))
                            edgeindex.append(edge_count-nskip)
                            G.add_edges(([ tuple([max(keepspecies)+1+edge_count,edges[k]]) ] )) 
                            #weight_new.append(diagofhat[i])
                            weight_new.append(float(uncvec[i]))
                            stoich_new.append(stoich[k])
#                            weight_new.append(provenance[i]+diagofhat[i])
                        for m in range(len(edges2)):
                            keepset.add(edges2[m])
                            keepset.add(max(keepspecies)+1+edge_count)
                            reactionlist.append((edges,edges2))
                            edgeindex.append(edge_count-nskip)
                            G.add_edges(([ tuple([edges2[m],max(keepspecies)+1+edge_count]) ] )) 
                            weight_new.append(float(uncvec[i]))
                            stoich_new.append(stoich2[m])
#                            weight_new.append(1.0/float(uncvec[i]))
#                            weight_new.append(provenance[i]+diagofhat[i])
                    else:
                        addedreaction=False
                        for j in range(len(reactionlist)):
                            if reactionlist[j]==(edges,edges2):
                                if not addedreaction:
                                    nskip+=1
#                                    edge_names[edgeindex[j]]=edge_names[edgeindex[j]]
                                    if (float(uncvec[i])/(4.184))<weight_new[j]: 
                                        weight_new[j]=(float(uncvec[i])/(4.184)) 
                                        edge_names[edgeindex[j]]=D.reactions[i].ATcTrxnID + ' ' + edge_names[edgeindex[j]]
                                    else:
                                        edge_names[edgeindex[j]]=edge_names[edgeindex[j]]+ ' ' +D.reactions[i].ATcTrxnID
                                    edge_infl[edgeindex[j]].append(diagofhat[i])
                                    addedreaction=True
#                                weight_new[j]=weight_new[j]+(1.0/float(uncvec[i]))
                else:
                    from sys import exit
                    print(AdjArray[i])
                    print(i)
                    exit('broken edge')
            weight_new=np.asarray(weight_new)
            minval=0.00001
            for a in np.nditer(weight_new, op_flags=['readwrite']):
                if a>0 and a<minval:
                    a[...]=minval
                elif np.abs(a)<minval:
                    a[...]=-minval
            weight_new=np.square(np.divide(np.multiply(np.asarray(stoich_new),weight_new),4.184))
            G.es['weight'] =np.ndarray.tolist(weight_new)
            
            keepvertex=keepspecies+list(range(max(keepspecies)+1,max(keepspecies)+1+n_edges))
            G=G.subgraph(keepset)
            node_names=[]
            for i in keepspecies:
                node_names.append(vertex_names[i])
            node_names=(node_names+edge_names)
            tokeep=set()
            
            displayed_spec_count = len(keepspecies)
            G.vs['label'] = node_names
            G.vs['influence'] = [[math.inf]]*displayed_spec_count+ edge_infl
            Hfuncsub=[]
            for i in range(len(keepspecies)):
                if keepspecies[i]>finalvertex-1:
                    Hfuncsub.append(-1000)
                else:
                    Hfuncsub.append(Hfunc[keepspecies[i]])
            for i in range(n_edges):
                Hfuncsub.append(-1001)
            G.vs['weight'] = Hfuncsub
            print('Lets get distances')
            #print(G.get_shortest_path(0))
            npairs=0
            runningtotal=0
            #print(G.shortest_paths_dijkstra(0,None,None,3)[0])
            pairs=[]
            for i in range(D.subnetvertices[x]+D.nterminators):
                paths=G.shortest_paths_dijkstra(i,None,G.es['weight'],3)[0]
                #print(paths[i:D.subnetvertices[x]+D.nterminators])
                for j in range(i+1,D.subnetvertices[x]+D.nterminators):
                    npairs+=1
                    runningtotal+=paths[j]
                    pairs.append(paths[j])
            import statistics
            print('Mean:',statistics.mean(pairs))
            print('Median:',statistics.median(pairs))
            print('Mode:',statistics.mode(pairs))
            print(float(runningtotal)/float(npairs))
            print(runningtotal,npairs)
                    #print(paths[j])

def submatrix_adjplot(numspeciestodisplay=40, covarplot=False,provenanceplot=True,initialcutoff=1e-8):
    """ plot submatricies """
    import numpy as np
    np.set_printoptions(suppress=True,threshold=np.nan,linewidth=np.nan)
    import os.path
    import sys
    initialedge=0
    initialvertex=0
    hatmatdone=False
    if os.path.isfile('hatmat.json'):
        if len(sys.argv)>1: 
            D=VarDecomp()
            D.inpfile=D.input_var_decomp()
            D.lines=openandread(D.inpfile)
            D.parse()
        else:
            import jsonpickle
            txt=open('hatmat.json','r')
            D=jsonpickle.decode(txt.read())
            hatmatdone=True
    measuredvec=D.measurementlist()
    uncvec=D.ATcTunclist()
    for x in range(1):
#    for x in range(D.nsubnets):
        AdjArray=np.zeros((D.subnetedges[x]+D.nterminators+1,D.subnetvertices[x]+D.nterminators),dtype=eval(numpy_precision))
        finalvertex=initialvertex+D.subnetvertices[x]
        finaledge=initialedge+D.subnetedges[x]+1
        terminatorshift=-len(D.species)+D.nterminators+D.subnetvertices[x]
        for y in range(initialedge,finaledge):
            for z in range(len(D.reactions[y].species)):
                if ((D.reactions[y].species[z]-initialvertex)>D.subnetvertices[x]):
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)+terminatorshift]=D.reactions[y].coef[z]
                else:
                    AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)]=D.reactions[y].coef[z]
        for y in range(D.nterminators):
           AdjArray[finaledge+y][finalvertex+y]=1.0
        if D.subnetedges[x]>=D.subnetvertices[x]:
            N=D.subnetvertices[x]
            M=D.subnetedges[x]
            measuredvecinp=measuredvec[initialedge:finaledge]+measuredvec[D.nedges:D.nedges+D.nterminators]
            uncvecinp=uncvec[initialedge:finaledge]+uncvec[D.nedges:D.nedges+D.nterminators]
            Yvec=np.array(measuredvecinp,dtype=eval(numpy_precision))
            Zvec=np.array(uncvecinp,dtype=eval(numpy_precision))
            if not hatmatdone:
                Vmat=np.diag(np.divide(1.,Zvec))
                Hfrec, Hfunc, truecovar =SVDsolve(AdjArray,Yvec,Zvec) 
                Adjw=np.dot(Vmat,AdjArray)
                hatmatrix=np.dot(np.dot(Adjw,truecovar),Adjw.transpose())
                diagofhat=np.diag(hatmatrix)[0:M]
            else:
                Hfrec=np.array(D.Hfrec,dtype=eval(numpy_precision))
                Hfunc=np.array(D.Hfunc,dtype=eval(numpy_precision))
#                truecovar=np.array(D.truecovar,dtype=eval(numpy_precision))
                diagofhat=np.array(D.diagofhat,dtype=eval(numpy_precision))
            from itertools import chain
            vertex_names = [ D.species[i].descriptor for i in chain(range(initialvertex,finalvertex), range(D.nvertices,D.nvertices+D.nterminators)) ]
            minval=10E-3
            import igraph 
            if covarplot:
                print('plotting based on covariance')
                AdjArray2=np.copy(AdjArray)
                for a in np.nditer(AdjArray2, op_flags=['readwrite']):
                    if a>minval:
                        a[...]=1
                    elif np.abs(a)>minval:
                        a[...]=-1
                    else:
                        a[...]=0
                AdjT=AdjArray2.transpose()
                Covar=np.dot(AdjT,AdjArray2)
                conn_indices = np.where(Covar) # get the row, col indices of the non-zero elements in your adjacency matrix
                weights = truecovar[conn_indices] # get the weights corresponding to these indices 
                edges = list(zip(*conn_indices)) # a sequence of (i, j) tuples, each corresponding to an edge from i -> j
                # initialize the graph from the edge sequence Directed adds undesired arrows
                G1 = igraph.Graph(edges=edges, directed=False)
                G1.es['weight'] = weights
#            for speciesofinterest in range(0,30):
            else:
#                print('plotting based on provenance of enthalpy assigned to all species in reaction equally')
                print('plotting based on influences')
#                Aw_inv=np.dot(np.dot(truecovar,AdjArray.transpose()),Vmat).transpose()
            while True:
                atctid=input('Give the atctid of the species of interest: ')
                if RepresentsInt(atctid):
                    speciesofinterest=int(atctid)
                else:
                    speciesofinterest=0
                    for i in range(N):
                        if str(D.species[i])==atctid:
                            speciesofinterest=i
                            print('species:' +str(i))
                            break
                if float(speciesofinterest)<0:
                    break
#                speciesofinterest= int( input('Give the number corresponding to the species of interest above: ') )
                cutoff=initialcutoff
                if covarplot:
                    keepspecies=[]
                    norm=np.sqrt(np.abs(truecovar[speciesofinterest,speciesofinterest]))
                    relative_covar=[]
                    edge_names=[]
                    related_covars=0.0
                    for i in range(N):
                        speciescovar=np.sqrt(np.abs(truecovar[i,i]))
                        relative_covar.append(truecovar[speciesofinterest,i]/(norm*speciescovar))
                        related_covars=related_covars+speciescovar
                    while True:
                        primaryvertex=0
                        for i in range(N):
                            covari=np.abs(relative_covar[i])
                            if covari>0.9999999999 and primaryvertex==0:
                                primaryvertex=len(keepspecies)
                            elif covari>0.9999999999:
                                from sys import exit
                                exit('multiple primary vertices found for {}'.format( D.species[speciesofinterest].descriptor))
                            if covari>cutoff:
                                keepspecies.append(i)
                        if len(keepspecies)>numspeciestodisplay:
                            keepspecies=[]
                            cutoff=cutoff*1.02
                        else:
                            keepset=keepspecies
                            n_edges=0
                            break
                elif provenanceplot:
                    termlist = []
                    for y in range(D.nterminators):
                        termlist.append(D.species[len(D.species)-1-y].descriptor)


#                    running_extended_covar_mat=np.zeros((M),dtype=eval(numpy_precision))
#                    for i in range(M):
#                        running_extended_covar_mat[i]=np.square(Aw_inv[i,speciesofinterest])
#                    covartotal=np.sqrt(np.sum(running_extended_covar_mat))
#                    if(np.subtract((covartotal),Hfunc[speciesofinterest]))>10e-8:
#                        from sys import exit
#                        print('covariance from projection not equal to solution covariance')
#                        print(covartotal,Hfunc[speciesofinterest])
#                        exit('covariance from projection not equal to solution covariance')
                    keepspecies= set()
#                    provenance=np.divide((running_extended_covar_mat),np.square(covartotal))
                    import sys
#                    cutoff_hat=(N/M)*2
                    cutoff_hat =initialcutoff
                    n_edges=0
#                    provlist=np.flipud(np.argsort(provenance))
#                    diagofhat2=np.where(np.abs(AdjArray[0:M,speciesofinterest])>0.0,diagofhat,np.zeros(M,dtype=eval(numpy_precision)) )
                    diagofhat2=np.copy(diagofhat)
                    inflist=np.flipud(np.argsort(diagofhat2))
                    edgelist=[]

                    sys.path.insert(0, '..')
                    from chemicalformulaparse import parseformula
                    interest_aqueous=False
                    interest_elements = parseformula([vertex_names[speciesofinterest]])[0]
                    if 'aq' in vertex_names[speciesofinterest]:
                        interest_aqueous=True
                    species_elements = []
                    aqueous = []
                    for i in range(len(vertex_names)):
                        species_elements.append(parseformula([vertex_names[i]])[0])
                        if 'aq' in vertex_names[i]:
                            aqueous.append(True)
                        else:
                            aqueous.append(False)
                    i=0                         
                    while i < len(inflist):
                        conn_indices = np.where(AdjArray[inflist[i]]) # get the row, col indices of the non-zero elements in your adjacency matrix
                        if any(c in species_elements[k] for k in conn_indices[0] for c in interest_elements): # and diagofhat[inflist[i]]>cutoff_hat:
                            edgelist.append(inflist[i])
                            n_edges+=1
                            for j in conn_indices:
                                for k in j:
                                    keepspecies.add(k)
                        if inflist[i]<cutoff_hat:
                            break
                        i+=1            
                    #add terminators that contain elements in common w/ the species of interest
                    newtermlist = []
                    
                    for term in termlist:
                         if any(i in parseformula([term])[0] for i in interest_elements):
                            for k in range(len(vertex_names)):
                                if vertex_names[k] == term and term not in keepspecies and interest_aqueous==aqueous[k]: #in vertex_names prev
                                    edgelist.append(k)
                                    conn_indices = np.where(AdjArray[k])
                                    n_edges+=1
                                    for j in conn_indices:
                                        for p in j:
                                            keepspecies.add(p)
                                    keepspecies.add(k)
                                    newtermlist.append(term)
                    termlist = newtermlist          


                    keepspecies=sorted(list(keepspecies))
                    for l in range(len(keepspecies)):
                        if keepspecies[l]==speciesofinterest:
                            primaryvertex=l
                    G = igraph.Graph(directed=True)
                    G.add_vertices(max(keepspecies)+1+n_edges)
                    weight_new=[]
                    edge_count=-1
                    edge_names=[]
                    edge_infl=[]
                    reactionlist=[]
                    keepset=set()
                    edgeindex=[]
                    nskip=0
                    for i in edgelist:
                        edge_count+=1
                        conn_indices = np.where(AdjArray[i]) # get the row, col indices of the non-zero elements in your adjacency matrix
                        edges = list(zip(*conn_indices)) # a sequence of (i, j) tuples, each corresponding to an edge from i -> j
                        for j in range(len(edges)):
                            edges[j]=int(edges[j][0])
                        j=0
                        edges2=[]
                        while j<len(edges): 
                            if AdjArray[i,edges[j]]<0:
                                edges2.append(edges[j])
                                del(edges[j])
                            else:
                                j+=1
                        if len(edges)>0 and len(edges2)>0:
                            if (edges,edges2) not in reactionlist:
#                                edge_names.append('')
                                edge_names.append(str(D.reactions[i].ATcTrxnID))
#                                edge_names.append(D.reactions[i].ATcTrxnID)
                                edge_infl.append([diagofhat[i]])
                                for k in edges:
                                    keepset.add(k)
                                    keepset.add(max(keepspecies)+1+edge_count) 
                                    reactionlist.append((edges,edges2))
                                    edgeindex.append(edge_count-nskip)
                                    G.add_edges(([ tuple([max(keepspecies)+1+edge_count,k]) ] )) 
                                    weight_new.append(diagofhat[i])
#                                    weight_new.append(1.0/float(uncvec[i]))
#                                    weight_new.append(provenance[i]+diagofhat[i])
                                for m in edges2:
                                    keepset.add(m)
                                    keepset.add(max(keepspecies)+1+edge_count)
                                    reactionlist.append((edges,edges2))
                                    edgeindex.append(edge_count-nskip)
                                    G.add_edges(([ tuple([m,max(keepspecies)+1+edge_count]) ] )) 
                                    weight_new.append(diagofhat[i])
#                                    weight_new.append(1.0/float(uncvec[i]))
#                                    weight_new.append(provenance[i]+diagofhat[i])
                            else:
                                addedreaction=False
                                for j in range(len(reactionlist)):
                                    if reactionlist[j]==(edges,edges2):
                                        if not addedreaction:
                                            nskip+=1
#                                            edge_names[edgeindex[j]]=edge_names[edgeindex[j]]
                                            if diagofhat[i]>weight_new[j]: 
                                                weight_new[j]=diagofhat[i]
                                                edge_names[edgeindex[j]]=D.reactions[i].ATcTrxnID + ' ' + edge_names[edgeindex[j]]
                                            else:
                                                edge_names[edgeindex[j]]=edge_names[edgeindex[j]]+ ' ' +D.reactions[i].ATcTrxnID
                                            edge_infl[edgeindex[j]].append(diagofhat[i])
                                            addedreaction=True
#                                        weight_new[j]=weight_new[j]+(1.0/float(uncvec[i]))
                        else:
                            from sys import exit
                            print(AdjArray[i])
                            print(i)
                            exit('broken edge')
                    weight_new=np.asarray(weight_new)
                    if covarplot:
                        norm=np.sqrt(np.abs(truecovar[speciesofinterest,speciesofinterest]))
                        relative_covar=[]
                        for i in range(N):
                            speciescovar=np.sqrt(np.abs(truecovar[i,i]))
                            relative_covar.append(truecovar[speciesofinterest,i]/(norm*speciescovar))
                        weight_new=np.divide(weight_new,np.sum(weight_new))
                        weight_new=np.divide(weight_new,np.max(weight_new))
                    minval=0.1
                    for a in np.nditer(weight_new, op_flags=['readwrite']):
                        if a>0 and a<minval:
                            a[...]=minval
                        elif np.abs(a)<minval:
                            a[...]=-minval
                    G.es['weight'] =np.ndarray.tolist(weight_new)
                    
                    keepvertex=keepspecies+list(range(max(keepspecies)+1,max(keepspecies)+1+n_edges))
                    G=G.subgraph(keepset)
                else:
                    from sys import exit
                    exit('graph plotting type not recognized on submatrix_adjplot')
                node_names=[]
                for i in keepspecies:
                    node_names.append(vertex_names[i])
                node_names=(node_names+edge_names)
                
                if covarplot:
                    G = G1.subgraph(keepspecies)
                    G.delete_edges([(primaryvertex,primaryvertex)])
                    edges = [e.tuple for e in G.es]
                    weight_new=np.zeros(len(edges),dtype=eval(numpy_precision))
                    k=0
                    for i,j in edges:
                        if i==primaryvertex:
                            weight_new[k]=relative_covar[keepspecies[j]]
                        elif j==primaryvertex:
                            weight_new[k]=relative_covar[keepspecies[i]]
                        else:
                            weight_new[k]=np.divide(np.sum(relative_covar[keepspecies[i]],\
                                    relative_covar[keepspecies[j]]),2.0)
                        k+=1
                    k=0
                    weight_new=np.divide(weight_new,np.subtract(np.max(weight_new),np.min(weight_new)))
                    minval=0.1
                    for a in np.nditer(weight_new, op_flags=['readwrite']):
                        if a>0 and a<minval:
                            a[...]=minval
                        elif np.abs(a)<minval:
                            a[...]=minval
                    weight_new=np.ndarray.tolist(weight_new)
                    G.es['weight'] = weight_new
                tokeep=set()

                if provenanceplot:
                    for i in range(len(keepset)):
                        if keepspecies[i]==speciesofinterest:
#                            print(keepspecies[i],speciesofinterest,i)
                            primaryvertex=i
                            primaryvertexname=node_names[i]
                            break
                
                displayed_spec_count = len(keepspecies)
                G.vs['label'] = node_names
                G.vs['influence'] = [[math.inf]]*displayed_spec_count+ edge_infl
                Hfuncsub=[]
                for i in range(len(keepspecies)):
                    if keepspecies[i]>finalvertex-1:
                        Hfuncsub.append(-1000)
                    else:
                        Hfuncsub.append(Hfunc[keepspecies[i]])
                for i in range(n_edges):
                    Hfuncsub.append(-1001)
                G.vs['weight'] = Hfuncsub

                visual_style = {}
                # Choose the layout
#                visual_style["layout"] = G.layout_fruchterman_reingold(maxiter=1000, area=N**3, repulserad=N**3)
#                visual_style["layout"] = G.layout_kamada_kawai()
#                visual_style["layout"] = G.layout_drl()
#                visual_style["layout"] = G.layout_lgl()
                # remove unconnected nodes
                keepspecies=list(set(keepspecies) & tokeep)

               
               
                #find shortest dist to each terminator from every species
                #termlist created at beginning of method

                term_vert_pos = []
                
                for t in termlist:
                     for j in range(G.vcount()):
                         if G.vs["label"][j] == t:
                             term_vert_pos.append(j)
                             G.vs["dist to "+t] = G.shortest_paths_dijkstra(j,None,None,3)[0]

                interestID = vertex_names[speciesofinterest]
                for k in range(G.vcount()):
                    if interestID == G.vs['label'][k]:
                        speciesofinterest = k
                        break

                displayed_spec_count=G.vs['influence'].count([math.inf])
                                
                #remove some of the less influential vertices from the graph:

                G.vs["dist to spec of interest"]=G.shortest_paths_dijkstra(speciesofinterest,None,None,3)[0]

                keeprxns =[]
                keepspecies=[speciesofinterest]
                
                few_enough_verts=False
                specieschecked=[]
                print('T1')
                while few_enough_verts == False:
                    Gundir = G.as_undirected()
                    discard=[]
                    for i in range(len(termlist)):
                        dist = 0
                        while dist <8:
#                            for r in range(G.vcount()):#use neighbors to iterate over fewer verts? probably equal ops
                            for species in keepspecies:
                                if species not in specieschecked:
                                    specieschecked.append(species)
                                    for r in G.neighbors(species):#use neighbors to iterate over fewer verts? probably equal ops
                                        if r not in keeprxns:
                                            if max(G.vs['influence'][r])>cutoff_hat:
                                                if any(Gundir.are_connected(r,k) for k in keepspecies):
                                                    if any(G.vs['dist to '+termlist[i]][r] <= G.vs['dist to '+termlist[i]][j] for j in G.neighbors(r) if j in keepspecies):
                                                        keeprxns.append(r)
                                            else:
                                                discard.append(r)
                                        neigh = G.neighbors(r,3)
                                        if len(neigh)==1 and len(G.neighbors(neigh[0]))==2 and r not in termlist and r!=speciesofinterest:
                                            discard.append(r)
                                            discard.append(neigh[0])
                                    for s in range(G.vcount()):
                                        if s not in keepspecies:
                                            if any(Gundir.are_connected(k,s) for k in keeprxns): 
                                                keepspecies.append(s)
                            dist+=2
                    nonterms = [l for l in keepspecies if l not in term_vert_pos]
                    print('T2')
                    for r in range(len(nonterms)):
                        for k in range(len(G.vs)):
                            if k not in keeprxns:
                                if max(G.vs['influence'][k])>cutoff_hat: 
                                    if Gundir.are_connected(nonterms[r], k) and Gundir.are_connected(term_vert_pos[i],k):
                                        keeprxns.append(k)
                    for s in range(G.vcount()):
                        if s not in keepspecies:
                            if any(Gundir.are_connected(k,s) for k in keeprxns):
                                keepspecies.append(s)
                    if len(keepspecies)+len(keeprxns) < numspeciestodisplay and len(keepspecies) > 1:
                        few_enough_verts=True
                    else:
                        cutoff_hat *= 1.02
                        G=G.subgraph(l for l in range(G.vcount()) if l not in discard)
                        keeprxns=[]
                        term_vert_pos = []
                        for s in range(G.vcount()):
                            if G.vs['label'][s] == interestID:
                                speciesofinterest=s
                                G.vs['dist to spec of interest']= G.shortest_paths_dijkstra(s,None,None,3)[0]
                            elif G.vs['label'][s] in termlist:
                                term_vert_pos.append(s)
                                G.vs["dist to "+G.vs['label'][s]] = G.shortest_paths_dijkstra(s,None,None,3)[0]
                        keepspecies =[speciesofinterest]
                

                G=G.subgraph(keepspecies+keeprxns)
                for s in range(G.vcount()):
                    if G.vs['label'][s] == interestID:
                        speciesofinterest=s

                for r in range(len(keepspecies),len(keepspecies+keeprxns)):
                    influlist = []
                    oldrxns = G.vs['label'][r].split(' ')
                    newrxns = ""
                    for k in range(len(G.vs['influence'][r])):
                          if G.vs['influence'][r][k] > cutoff_hat:
                            influlist.append(G.vs['influence'][r][k])
                            newrxns+=oldrxns[k]+" "
                    G.vs[r]['influence']=influlist
                    G.vs[r]['label']=newrxns

                print('{} had {} species within a cutoff of {}%'.\
                       format(interestID,len(keepspecies),cutoff*100))
                print('list of related species {}'.format(G.vs['label']))
#                print('list of related species {}'.format(node_names[0:len(keepspecies)]))

                #for e in G.es:
                 #   G.es['weight']= G.vs[e.source]['influence']+G.vs[e.target]['influence']
                G.es['width'] = np.abs(np.multiply(3,G.es['weight']))

                
                
                Hfuncsub = G.vs['weight']
                newminunc=min( i for i in Hfuncsub if i >0)
                newmaxunc=max(Hfuncsub)
                listofshapes=[]
                vertexsizeoverride=[]
                for i in range(G.vcount()):
                    if Hfuncsub[i]==-1001:
                        Hfuncsub[i]=newmaxunc
                        listofshapes.append("rectangle")
                        vertexsizeoverride.append(True)
                    else:
                        listofshapes.append("circle")
                        vertexsizeoverride.append(False)
                    if Hfuncsub[i]==-1000:
                        Hfuncsub[i]=newminunc
                G.vs['weight'] = np.ndarray.tolist(np.reciprocal(Hfuncsub))

                outdegree=G.outdegree()
                vertexsize=[i/max(outdegree)*20+25 for i in outdegree]
                maxvsize=np.multiply(np.max(vertexsize),1.5)
                minvsize=np.divide(np.min(vertexsize),1.5)
                sizeforbins =  np.linspace(minvsize,maxvsize, 5)  
                for i in range(len(vertexsize)):
                    vertexsize[i]=np.divide(maxvsize,Hfuncsub[i])
                bins =  np.linspace(min(vertexsize), max(vertexsize), 5)  
                digitized_vertexs =  np.digitize(vertexsize, bins)
                vsize_final = [sizeforbins[i-1] for i in digitized_vertexs]
                for i in range(len(vertexsizeoverride)):
                    if vertexsizeoverride[i]:
                        pass
#                        vsize_final[i]=0

#                G.vs['weight']= vsize_final
                visual_style["vertex_size"] = vsize_final
                visual_style["bbox"] = (800,800)
                visual_style["margin"] = 100
#                listofshapes=[]
#                for i in range(len(keepspecies)):
#                    listofshapes.append("circle")
#                for i in range(n_edges):
#                    listofshapes.append("rectangle")
                visual_style["vertex_shape"]=(listofshapes)

                colours = ['#337aff','#46ff33','#fecc5c','#a31a1c']
                secondvertexcolor='#808080' 
                v_color=[]
                from math import floor
                if provenanceplot:
#                    for i in range(G.vcount()):
 #                       if G.vs['label'][i] == primaryvertexname:
  #                          primaryvertex=i
   #                         break
                    primaryvertex=speciesofinterest
                for i in range(len(vertexsize)):
                    G2=G.as_undirected()
                    try:
                        dist= int(G2.shortest_paths_dijkstra(primaryvertex,i)[0][0])
                    except OverflowError:
                        dist=len(colours)
                    if provenanceplot and dist<len(colours)*2 or covarplot and dist<len(colours):
                        if listofshapes[i]=="rectangle":
                            v_color.append(secondvertexcolor)
                        else:
                            if provenanceplot:
                                v_color.append(colours[floor(dist/2)])
                            else:
                                v_color.append(colours[int(dist)])
                    else:
                        if listofshapes[i]=="rectangle":
                            v_color.append(secondvertexcolor)
                        else:
                            v_color.append(colours[-1])
                G.vs["color"]=v_color
                
                for v in range(G.vcount()):
                    if max(G.vs[v]['influence'])<math.inf:
                        G.vs[v]['label']+=(str(round(max(G.vs[v]['influence']),2)*100))
                

                # Also color the edges
                for ind, color in enumerate(G.vs["color"]):
                    edges = G.es.select(_source=ind)
                    edges["color"] = [color]
                
                # curve the edges
#                visual_style["edge_curved"] = True
                visual_style["edge_curved"] = False
               
#                visual_style["vertex_label_size"]=12
                 # Plot the graph
#                igraph.plot(G, **visual_style)
                igraph.plot(G, interestID+'.pdf', **visual_style)
        initialedge=finaledge
        initialvertex=finalvertex

def submatrix(NEreaction1):
    """ Solve submatricies as specified by ATcT"""
    D=VarDecomp()
    D.inpfile=D.input_var_decomp()
    D.lines=openandread(D.inpfile)
    D.parse()
    import numpy as np
    np.set_printoptions(suppress=True,threshold=np.nan,linewidth=np.nan)
    initialedge=0
    initialvertex=0
    measuredvec=D.measurementlist()
    uncvec=D.ATcTunclist()
    sympy=False
    printmismatches=0
#    D.printreactions()
    for x in range(D.nsubnets):
        if sympy:
            from sympy.matrices import zeros
            from sympy import S
            Adjmatrix=zeros(D.subnetedges[x],D.subnetvertices[x])
        else:
            AdjArray=np.zeros((D.subnetedges[x],D.subnetvertices[x]),dtype=eval(numpy_precision))
            AdjArray=np.zeros((D.subnetedges[x]+D.nterminators,D.subnetvertices[x]+D.nterminators),dtype=eval(numpy_precision))
        finalvertex=initialvertex+D.subnetvertices[x]
        finaledge=initialedge+D.subnetedges[x]
        terminatorshift=-len(D.species)+D.nterminators+D.subnetvertices[x]
        finalvertex=initialvertex+D.subnetvertices[x]
        finaledge=initialedge+D.subnetedges[x]
        for y in range(initialedge,finaledge):
            for z in range(len(D.reactions[y].species)):
                if sympy:
                    Adjmatrix[y-initialedge, D.reactions[y].species[z]-initialvertex]=S(D.reactions[y].coef[z])
                else:
                    for y in range(initialedge,finaledge):
                        for z in range(len(D.reactions[y].species)):
                            if ((D.reactions[y].species[z]-initialvertex)>D.subnetvertices[x]):
                                AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)+terminatorshift]=D.reactions[y].coef[z]
                            else:
                                AdjArray[y-initialedge][(D.reactions[y].species[z]-initialvertex)]=D.reactions[y].coef[z]
                    for y in range(D.nterminators):
                        AdjArray[finaledge+y][finalvertex+y]=1.0
        if D.subnetedges[x]>=D.subnetvertices[x]:
            if condition_num_print:
                print('Condition Number of subnet %s= %s' % (x,np.linalg.cond(AdjArray)))
            if sympy:
                measuredvecinp=measuredvec[initialedge:finaledge]
                uncvecinp=uncvec[initialedge:finaledge]
            else:
                measuredvecinp=measuredvec[initialedge:finaledge]+measuredvec[D.nedges:D.nedges+D.nterminators]
                uncvecinp=uncvec[initialedge:finaledge]+uncvec[D.nedges:D.nedges+D.nterminators]
            Yvec=np.array(measuredvecinp,dtype=eval(numpy_precision))
            Zvec=np.array(uncvecinp,dtype=eval(numpy_precision))
            if sympy:
                Hfrec, Hfunc=Gsolve_arb(Adjmatrix,measuredvecinp,uncvecinp)
            else:
                if solvehatmat:
                    Hfrec, Hfunc, truecovar, hatmat =SVDsolve(AdjArray,Yvec,Zvec) 
                    print(Hfrec)
                    m, n=AdjArray.shape
                    Vmat=np.diag(np.reciprocal(1.,Zvec))
                    Adjw=np.dot(Vmat,AdjArray)
                    while True:
                        if x>0:
                            break
                        atctid=input('Give the atctid of the species of interest: ')
                        if RepresentsInt(atctid):
                            speciesofinterest=int(atctid)
                        else:
                            speciesofinterest=-1
                            for i in range(n):
                                if str(D.species[i+initialvertex])==atctid:
                                    speciesofinterest=i
                                    break
                        if float(speciesofinterest)<0:
                            break
                        tmpcovar=np.zeros(truecovar.shape,dtype=eval(numpy_precision))
                        for i in range(1,n):
                            tmpcovar[i,speciesofinterest]=truecovar[i,speciesofinterest]
                            tmpcovar[speciesofinterest,i]=truecovar[speciesofinterest,i]
                        hatmatrix=np.dot(np.dot(Adjw,tmpcovar),Adjw.transpose())
                        diagofhat=np.diag(hatmatrix)
                        diagofhat=np.divide(diagofhat,(np.max(diagofhat)-np.min(diagofhat)))
                        mostinfluential=np.flipud(np.argsort(np.abs(diagofhat)))
                        for i in range(20):
                            rxnid, measid=str(D.reactions[mostinfluential[i]+initialedge]).split('.')
                            rxnid=int(rxnid)
                            measid=int(measid)
                            rlist1 = [ r.reactionnum for r in NEreaction1]
                            r1ind=np.array(rlist1,dtype=np.int)
                            rxnindex=np.squeeze(np.ravel(np.where(r1ind==rxnid)))
                            foundit=False
                            if rxnindex.size>0:
                                for y in range(len(NEreaction1[rxnindex].measurement)):
                                    if NEreaction1[rxnindex].measurement[y].measurementnum==measid:
                                        mindex=y
                                        foundit=True
                                        break
                            if foundit:
                                idstr=str(rxnid)+'.'+str(measid)
                                print('{0:10} {1:10} {2:}\n{4:24}{3:}'.format(idstr,diagofhat[mostinfluential[i]], NEreaction1[rxnindex].reactionstr, NEreaction1[rxnindex].measurement[mindex],''))
                else:
                    Hfrec, Hfunc, truecovar =SVDsolve(AdjArray,Yvec,Zvec) 
#                Hfrec, Hfunc=GJsolve(AdjArray,Yvec,Zvec)
#                Hfrec=sparsesolve(AdjArray,Yvec,Zvec)
            maxerror=0
#            for y in range(len(Hfrec)):
#                print(Hfrec[y])
            for y in range(len(Hfrec)):
                y1=y+initialvertex
#                mindiff=1.0e-06
                mindiff=float(D.species[y1].unc)*0.001
                maxerror=max(abs(float(Hfrec[y])-float(D.species[y1].dh)),maxerror)
                if abs(float(Hfrec[y])-float(D.species[y1].dh))>mindiff: #or abs(float(Hfunc[y])-float(D.species[y1].unc)*1.014198783)>mindiff:
                    if printmismatches==0:
                        print ('# : calc Hf : original Hf +- unc, : Ratio of unc to Difference')
                    printmismatches=1
                    ratioofdiff=round(abs(float(Hfrec[y])-float(D.species[y1].dh))/float(D.species[y1].unc)*100,0)
                    ratioofdiff1=round(abs(float(Hfunc[y])-float(D.species[y1].unc))/float(D.species[y1].unc)*100,0)
#                    print (D.species[y1].number+1,': ',Hfrec[y],':',Hfrec1[y],' : ',D.species[y1].dh,'+-',D.species[y1].unc,' : ',ratioofdiff,' : ', ratioofdiff1)
#                    print (D.species[y1].number+1,': ',Hfrec[y],' : ',D.species[y1].dh,'+-',D.species[y1].unc,' : ',ratioofdiff)
                    print (D.species[y1].number+1,': ',Hfrec[y],'+-',Hfunc[y],' : ',D.species[y1].dh,'+-',D.species[y1].unc,' : ',ratioofdiff ,' : ', ratioofdiff1)
#        print(maxerror)
        initialedge=finaledge
        initialvertex=finalvertex

def fullmatrix():
    """ Solve Full matrix: for SVD on version 028 and 118 this gives the same solutions, although uncertainties are untested. In principle it is simpler to construct the full matrix using this method"""
    D=VarDecomp()
    D.inpfile=D.input_var_decomp()
    D.lines=openandread(D.inpfile)
    D.parse()
    measuredvec=D.measurementlist()
    uncvec=D.ATcTunclist()
    import numpy as np
    np.set_printoptions(suppress=True,threshold=np.nan,linewidth=np.nan)
    Adjmatrix=vec2matrix(D.adjacencylist,D.nedges,D.nvertices)
    Yvec=np.array(measuredvec,dtype=eval(numpy_precision))
    Zvec=np.array(uncvec,dtype=eval(numpy_precision))
#    Hf=sparsesolve(Adjmatrix,Yvec,Zvec)
    if solvehatmat:
        Hfrec, Hfunc, truecovar, hatmat =SVDsolve(Adjmatrix,Yvec,Zvec) 
    else:
        Hfrec, Hfunc, truecovar =SVDsolve(Adjmatrix,Yvec,Zvec) 
#    Hfrec, Hfunc =GJsolve(Adjmatrix,Yvec,Zvec) 
#    Hfsol=ATcTnormeq(Adjmatrix,Yvec,Zvec,pinv=1)
    printmismatches=0
    for x in range(len(Hfrec)):
        mindiff=float(D.species[x].unc)*0.01
#        if abs(float(Hfrec[x])-float(D.species[x].dh))>float(Hfunc[x]) or abs(float(Hfrec[x])-float(D.species[x].dh))>float(D.species[x].unc):
        if float(D.species[x].unc)<1e-07:
            print(D.species[x].dh,D.species[x].unc)
            print('Failed on species ' + str(x))
        elif abs(float(Hfrec[x])-float(D.species[x].dh))>mindiff:
            if printmismatches==0:
                print ('# : calc Hf +- unc : original Hf +- unc, : Ratio of unc to Difference')
            printmismatches=1
            ratioofdiff=abs(float(Hfrec[x])-float(D.species[x].dh))/float(D.species[x].unc) 
            print (D.species[x].number+1,': ',Hfrec[x],'+-',Hfunc[x],' : ',D.species[x].dh,'+-',D.species[x].unc,' : ',ratioofdiff)
#    print(Hfrec)

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def setprec(prec="np.float64"):
    global numpy_precision
    numpy_precision=prec

def setprints(CN=False,HM=False):
    global solvehatmat, condition_num_print
    solvehatmat=False
    condition_num_print=False
    if CN:
        condition_num_print=True
    if HM:
        solvehatmat=True

def main():
    setprec()
# Settings
# Doesn't work with linalg routines
#    numpy_precision="np.longdouble"
    solvehatmat=True
    condition_num_print=False
    solvesub='T'
    solvehatmat_input='T'
    HM=True
    import sys
    #sys.path.insert(0, '..')
    from NetworkEncyclopedia import NetworkEncyclopedia, Reaction, Species, Measurement, openandread, ne_parse_settings, ne_filenames
# parameters
    global typoprint, parsereaction, checkallatctid, querydebug, printatctiderrors, stringmaxlength
    global maxcoefnum
    typoprint=0 # If typoprint = 1 then typos in input are printed out
    parsereaction=0 # If parsereaction=1 then reaction strings are parsed
    querydebug=0 # If querydebug=1 then the failed queries are printed, if >1 then ALL queries are printed
    checkallatctid=0 # if checkallatctid!=0 then all atctids are checked via query 
    printatctiderrors=0 # if printatctiderrors=1, print the errors, otherwise ignore
    stringmaxlength=10000 # number of characters that can be read in a reaction string not useful because ATcT's is size based on charlen
    maxcoefnum=100000 # Number of coefficent preceeding species in reactions ATcT parser has a hardlimit on this
    printfixedatctids=0 # If printfixedatctids=1 then mismatches between NE and WONE are printed
    dbupload=0 # if dbupload=1 then we upload the successfully parsed stuff to the MYSQL database, else don't
# Lets automatically set up how we validate
    integritycheckreaction=1
#    integritycheckreaction= int(input('Give the level of reaction checking desired 0= none, \
#            1=by reaction number, 2=by reaction string, 3=force ATcTID lookups and by reaction string'))
    if integritycheckreaction>=2:
        parsereaction=1 # If parsereaction=1 then reaction strings are parsed
    if integritycheckreaction>=3:
        checkallatctid=1 # if checkallatctid!=0 then all atctids are checked via query 
        printatctiderrors=int(input('Give the level of printing detail 1=print ATcTID errors; 0=do not print: '))
        typoprint= int(input('Give the level of typo printing 1=print, 0=do not print: '))
# This function sets all of the settings used subsequently
    ne_parse_settings(typoprint, parsereaction, checkallatctid, querydebug, printatctiderrors, stringmaxlength, maxcoefnum)
    filename1='NetworkEncyclopedia.txt'
    filename2='WONetworkEncyclopedia.txt'
    filename3='SavedNetworkEncyclopedia.txt'
    ne_filenames(filename1,filename2,filename3)
    NE1=NetworkEncyclopedia(filename1)
    NE1.readfiletype()
    from operator import attrgetter, itemgetter
    NEreaction1= [reaction for reaction in sorted(NE1.reaction, key=attrgetter('reactionnum'))]

#    solvesub=input('Solve submatricies as specified by ATcT code? [T,f]: ' )
#    solvehatmat_input=input('Generate hatmatrix? [T,f]: ' )
    if 'f' in solvehatmat_input.lower():
        HM=False
    setprints(HM=HM)
    if 'f' in solvesub.lower():
        fullmatrix()
    else:
        submatrix(NEreaction1)

if __name__=="__main__":
    main()

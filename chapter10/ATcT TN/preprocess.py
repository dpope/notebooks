#!/usr/bin/env python
""" Designed to preprocess the variancedecomp output into a pickle for later loading to plot the Adjacency graph """

from __future__ import absolute_import, division, print_function, unicode_literals
from builtins import (bytes, str, open, super, range,
                              round, input, int, pow, object) # need python2 zip
def main():
    from hatmat import submatrix_preparse , setprec , setprints
    setprec()
    setprints()
    submatrix_preparse()

if __name__=="__main__":
    main()

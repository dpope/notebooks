nodes = (0,1,2,3,4,5)
node_attribute = ('A','B','A','A','B','B')
edges = ((0,1),(0,4),(1,2),(1,4),(2,3),(3,4),(3,5))
edge_weights = (0.9,1.5,1.3,1.0,1.1,1.2,1.1)

#Graph
G=nx.Graph()
for node in nodes: G.add_node(node)
for edge in edges: G.add_edge(*edge)
print('Graph \n nodes \t',  G.nodes(), '\n edges \t', G.edges(), '\n')

#Node and Edge Attributes
for i in range(len(nodes)): G.add_node(nodes[i], type=node_attribute[i])
for i in range(len(edges)): G.add_edge(*edges[i], weight=edge_weights[i])
print('Graph Attributes \n nodes \t',  G.nodes(data=True), '\n edges \t', G.edges(data=True), '\n')

#Directed Graph
D=nx.DiGraph()
for node in nodes: D.add_node(node)
for edge in edges: D.add_edge(*edge)

#subGraph of "A" nodes
sub_nodes = list(node for node, data in G.nodes(data=True) if data.get("type") == "B")
sub_graph_A = G.subgraph(sub_nodes)
print('subGraph \n nodes \t',  sub_graph_A.nodes(), '\n edges \t', sub_graph_A.edges(), '\n')

#MultiGraph
M=nx.MultiGraph()
for node in nodes: M.add_node(node)
for edge in edges: M.add_edge(*edge)
M.add_edge(0,1)
print('MultiGraph \n nodes \t',  M.nodes(), '\n edges \t', M.edges(), '\n')